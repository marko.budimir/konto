package hr.ferit.markobudimir.konto

import android.app.Application
import hr.ferit.markobudimir.konto.di.viewmodelModule
import org.koin.android.BuildConfig
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class Konto: Application() {

    override fun onCreate() {
        super.onCreate()
        application = this

        startKoin {
            androidLogger(if (BuildConfig.DEBUG) Level.ERROR else Level.NONE)
            androidContext(this@Konto)
            modules(
                viewmodelModule
            )
        }
    }

    companion object{
        lateinit var application: Application
    }
}