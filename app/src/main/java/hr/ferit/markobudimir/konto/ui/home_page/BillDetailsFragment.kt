package hr.ferit.markobudimir.konto.ui.home_page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import hr.ferit.markobudimir.konto.databinding.FragmentBillDetailsBinding

class BillDetailsFragment : Fragment(){

    private lateinit var binding: FragmentBillDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBillDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

}