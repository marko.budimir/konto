package hr.ferit.markobudimir.konto.ui.home_page

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

private const val PAGE_NUMBER = 2

class HomePagePagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    override fun getItemCount(): Int = PAGE_NUMBER

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            1 -> UserDetailsFragment()
            else -> BillDetailsFragment()
        }
    }

}