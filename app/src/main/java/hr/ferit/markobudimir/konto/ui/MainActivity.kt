package hr.ferit.markobudimir.konto.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import hr.ferit.markobudimir.konto.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}