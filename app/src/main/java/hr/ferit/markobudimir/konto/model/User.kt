package hr.ferit.markobudimir.konto.model

data class User(
    val name: String?,
    val revenues: String?,
    val expenses: String?,
    val profit: String?
)
