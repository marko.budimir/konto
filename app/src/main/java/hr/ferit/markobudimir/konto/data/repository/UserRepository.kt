package hr.ferit.markobudimir.konto.data.repository

import androidx.lifecycle.LiveData
import hr.ferit.markobudimir.konto.model.User

interface UserRepository {

    fun getUserData(): LiveData<User>
}