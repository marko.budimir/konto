package hr.ferit.markobudimir.konto.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import hr.ferit.markobudimir.konto.model.User

private const val TAG = "database_error"

class UserRepositoryImpl : UserRepository {
    private val auth = Firebase.auth
    private var databaseRef = Firebase.database.reference

    override fun getUserData(): LiveData<User> {
        databaseRef = databaseRef.child("users").child(auth.uid.toString())
        val user: MutableLiveData<User> = MutableLiveData()

        val postListener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val name = snapshot.child("Name").getValue<String>()
                val revenues = snapshot.child("revenues").getValue<String>()
                val expenses = snapshot.child("expenses").getValue<String>()
                val profit = snapshot.child("profit").getValue<String>()
                user.value = User(name, revenues, expenses, profit)
            }

            override fun onCancelled(error: DatabaseError) {
                user.value = User("Error", "", "", "")
                Log.w(TAG, "Failed to read user data.", error.toException())
            }
        }
        databaseRef.addValueEventListener(postListener)

        return user
    }
}