package hr.ferit.markobudimir.konto.uiFF4A4A


import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import hr.ferit.markobudimir.konto.R
import hr.ferit.markobudimir.konto.databinding.FragmentLoginBinding

class LoginFragment: Fragment() {

    private lateinit var binding: FragmentLoginBinding
    private lateinit var auth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        auth = Firebase.auth
        if (auth.currentUser != null) { showHomePage() }

        binding.btnLogin.setOnClickListener { signIn()}
        return binding.root
    }

    private fun signIn() {
        val email = binding.etEmail.text.toString()
        val password = binding.etPassword.text.toString()

        if (TextUtils.isEmpty(email)) {
            binding.etEmail.error = resources.getString(R.string.et_email_error)
            return
        }
        if (TextUtils.isEmpty(password)) {
            binding.etPassword.error = resources.getString(R.string.et_password_error)
            return
        }

        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                showHomePage()
            }
            else {
                Toast.makeText(context, resources.getString(R.string.auth_error), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun showHomePage() {
        val action = LoginFragmentDirections.actionLoginFragmentToHomePageFragment()
        findNavController().navigate(action)
    }

}